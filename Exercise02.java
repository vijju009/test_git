import java.util.*;
public class Exercise02{
	public static void printIt(String name,String output){
		System.out.println(name);
		System.out.println(output);
	}
	public static List<Character> produceList(String str){
		List<Character> list = new ArrayList<>();
		for(int i=0;i<str.length();i++){
			list.add(str.charAt(i));
		}
		return list;
	}
	public static void nrzL(List<Character> list){
		Iterator<Character> iter = list.iterator();
		String nrz = "";
		while(iter.hasNext()){
			if(iter.next() == '0')
				nrz += "+";
			else
				nrz += "-";
		}
		printIt("NRZ-L",nrz);
	}
	public static void nrzI(List<Character> list){
		Iterator<Character> iter = list.iterator();
		String nrz = "-";
		while(iter.hasNext()){
			if(iter.next() == '1'){
				if(nrz.charAt(nrz.length()-1) == '-')
					nrz += "+";
				else
					nrz += "-";
			}
			else{
				if(nrz.charAt(nrz.length()-1) == '-')
					nrz += "-";
				else
					nrz += "+";
			}
		}
		nrz = nrz.substring(1,nrz.length());
		printIt("NRZ-I",nrz);
	}
	public static void bipolarAmi(List<Character> list){
		Iterator<Character> iter = list.iterator();
		int counter = 0;
		String bAmi = "";
		while(iter.hasNext()){
			if(iter.next() == '1'){
				if(counter%2 == 0){
					bAmi += "+";
					counter++;
				}
				else{
					bAmi += "-";
					counter++;
				}
			}
			else
				bAmi += "0";
		}
		printIt("Bipolar-Ami",bAmi);
	}
	public static void pseudoTernary(List<Character> list){
		Iterator<Character> iter = list.iterator();
		int counter = 0;
		String pTernary = "";
		while(iter.hasNext()){
			if(iter.next() == '0'){
				if(counter%2 == 0){
					pTernary += "+";
					counter++;
				}
				else{
					pTernary += "-";
					counter++;
				}
			}
			else
				pTernary += "0";
		}
		printIt("Pseudoternary", pTernary);
	}
	public static void main(String[] args) {		
		String str = args[0];
		List<Character> list = new ArrayList<>();
		list = produceList(str);
		nrzL(list);
		nrzI(list);
		bipolarAmi(list);
		pseudoTernary(list);		
	}
}