import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exercise01{	
	public static String parityBit(String str){
		List<Character> bits = new ArrayList<>();
		for(int i=0;i<str.length();i++){
			bits.add(str.charAt(i));
		}
		Iterator<Character> iter = bits.iterator();
		int counter = 0;
		while(iter.hasNext()){
			if(iter.next()== '1')
				counter++;
		}	
		if(counter%2 == 1)
			return "1";
		else
			return "0";
	}
	
	public static void main(String[] args) {	
		String str = args[0];
		assert(str.length()>= 4 && str.length()<= 32);
		String newString = parityBit(str);
		System.out.println(newString);
	}
}