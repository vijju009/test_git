import java.util.Scanner;

public class Exercise03 {
		public static String binaryAddition(String s1, String s2) {
		    if (s1 == null || s2 == null) return "";
		    int first = s1.length() - 1;
		    int second = s2.length() - 1;
		    StringBuilder sb = new StringBuilder();
		    int carry = 0;
		    while (first >= 0 || second >= 0) {
		        int sum = carry;
		        if (first >= 0) {
		            sum += s1.charAt(first) - '0';
		            first--;
		        }
		        if (second >= 0) {
		            sum += s2.charAt(second) - '0';
		            second--;
		        }
		        carry = sum >> 1;
		        sum = sum & 1;
		        sb.append(sum == 0 ? '0' : '1');
		    }
		    if (carry > 0)
		        sb.append('1');

		    sb.reverse();
		    return String.valueOf(sb);
		}

		public static String internetChecksum(String[] str){	
			String strNew[] = str;
			String ans = "";
			for(String s : strNew){
				if(ans.length() >4){
					if(ans.charAt(0) == '1'){
						ans = binaryAddition("1", ans.substring(1,ans.length()));
					}
				}
				ans = binaryAddition(ans, s);
			}
			if(ans.length() >4){
				if(ans.charAt(0) == '1'){
					ans = binaryAddition("1", ans.substring(1,ans.length()));
				}
			}
			return ans;
		}

		public static void main(String[] args) {
			String newString = internetChecksum(args);
			System.out.println(newString);
		}
}