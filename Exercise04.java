import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Exercise04 {
	public static int hammingDistance(String str){
		String stt[] = str.split(" ");
		List<Character> bits1 = new ArrayList<>();
		for(int i=0;i<stt[0].length();i++){
			bits1.add(stt[0].charAt(i));
		}
		Iterator<Character> iter1 = bits1.iterator();
		List<Character> bits2 = new ArrayList<>();
		for(int i=0;i<stt[1].length();i++){
			bits2.add(stt[1].charAt(i));
		}
		Iterator<Character> iter2 = bits2.iterator();
		int counter = 0;
		while(iter1.hasNext()){
			if(iter1.next() != iter2.next())
				counter += 1;
		}
		return counter; 
	}
	
	public static void main(String[] args) {
	    String str = args[0] + " " + args[1];
		int distance = hammingDistance(str);
		System.out.println(distance);
	}
}